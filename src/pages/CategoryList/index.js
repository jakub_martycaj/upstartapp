import React, { Component } from 'react';
import { Product } from './components/Product';
import { Sidebar } from './components/Sidebar';
import { Row, Col, Grid } from 'react-flexbox-grid';

export class CategoryList extends Component {
  componentDidMount = () => {
    this.props.fetchProducts();
  };

  onBrandChange = ({ target }) => {
    this.props.filterByBrand(target.value, target.checked);
  };

  onCategoryChange = ({ target }) => {
    this.props.filterByCategory(target.value, target.checked);
  };

  render() {
    return (
      <Grid>
        <Row>
          <Col xs={12} md={3}>
            <Sidebar
              categories={this.props.categories}
              brands={this.props.brands}
              onBrandChange={this.onBrandChange}
              onCategoryChange={this.onCategoryChange}
            />
          </Col>
          <Col xs={12} md={9}>
            <h2>Ceiling Fans</h2>
            <Row>
              {this.props.products.map(prod => (
                <Product key={prod.code} {...prod} />
              ))}
            </Row>
          </Col>
        </Row>
      </Grid>
    );
  }
}
