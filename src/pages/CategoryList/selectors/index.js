import { createSelector } from 'reselect';

const getProducts = state => state.products.results;

const getProductsFilter = state => state.products.filter;

const getFilters = filter =>
  Object.keys(filter).filter(key => filter[key] === true);

const filterProducts = (products, search, brandFilters, categoryFilters) =>
  products.filter(
    prod =>
      (brandFilters.length > 0
        ? brandFilters.includes(prod.manufacturer.toLowerCase())
        : true) &&
      (categoryFilters.length > 0
        ? categoryFilters.includes(prod.categoryID.toString())
        : true) &&
      (search.length > 2
        ? new RegExp(search.toLowerCase()).test(prod.name.toLowerCase())
        : true),
  );

export const getFilteredProducts = createSelector(
  getProducts,
  getProductsFilter,
  (products, filter) => {
    const categoryFilters = getFilters(filter.category);
    const brandFilters = getFilters(filter.brand);
    return filterProducts(
      products,
      filter.search,
      brandFilters,
      categoryFilters,
    );
  },
);
