import React from 'react';

import styled from 'styled-components';
import Collapsible from 'react-collapsible';

const SidebarContainer = styled.div`
  width: 100%;
  padding-right: 16px;
`;

const CollapsibleTrigger = styled.div`
  background-color: lightgrey;
  padding: 8px;
  font-size: 14px;
  font-weight: bold;
  border-radius: 4px;
  cursor: pointer;
`;

const List = styled.ul`
  margin: 8px 0;
  padding: 0;
  list-style: none;
`;

const ListItem = styled.li`
  padding: 8px;
  font-size: 14px;
`;

const Checkbox = styled.input.attrs({
  type: 'checkbox',
})`
  margin-right: 8px;
`;

export const Sidebar = ({
  categories = [],
  brands = [],
  onBrandChange,
  onCategoryChange,
}) => {
  return (
    <SidebarContainer>
      <h3>Refine By</h3>
      <Collapsible
        open
        trigger={<CollapsibleTrigger>Category</CollapsibleTrigger>}
      >
        <List>
          {categories.map(cat => {
            const id = `catFilter-${cat.id}`;
            return (
              <ListItem key={cat.id}>
                <Checkbox id={id} value={cat.id} onChange={onCategoryChange} />
                <label htmlFor={id}>{cat.name}</label>
              </ListItem>
            );
          })}
        </List>
      </Collapsible>
      <Collapsible
        open
        trigger={<CollapsibleTrigger>Brand</CollapsibleTrigger>}
      >
        <List>
          {brands.map(brand => {
            const id = `brandFilter-${brand}`;
            return (
              <ListItem key={brand}>
                <Checkbox id={id} value={brand} onChange={onBrandChange} />
                <label htmlFor={id}>{brand}</label>
              </ListItem>
            );
          })}
        </List>
      </Collapsible>
    </SidebarContainer>
  );
};
