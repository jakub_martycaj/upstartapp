import React from 'react';
import styled from 'styled-components';

import { Button } from '../../../../components/Button';
import { Col } from 'react-flexbox-grid';

const ProductGridItem = styled(Col)`
  padding: 16px;
  margin-left: -1px;
  margin-top: -1px;
  border: solid 1px lightgray;
`;

const ProductName = styled.h2`
  color: #727272;
  font-size: 14px;
`;

const ProductImage = styled.img`
  display: block;
`;

const ProductPrice = styled.h3`
  color: red;
  font-size: 16px;
  font-weight: normal;
`;

const currencySymbols = {
  INR: '₹',
};

export const Product = ({ name, images, price }) => {
  const imageUrl = `https://croma.com${images[0].url}`;
  return (
    <ProductGridItem xs={12} md={4}>
      <ProductImage alt={name} src={imageUrl} />
      <ProductName>{name}</ProductName>
      <ProductPrice>
        {currencySymbols[price.currencyIso]} {price.value}
      </ProductPrice>
      <Button>Add to cart</Button>
    </ProductGridItem>
  );
};
