import { callApi } from '../api';
import {
  PRODUCTS_LOADING,
  PRODUCTS_LOADED,
  FILTER_BY_BRAND,
  FILTER_BY_CATEGORY,
  FILTER_BY_SEARCH,
} from './ActionTypes';

export const fetchProducts = () => async dispatch => {
  dispatch({ type: PRODUCTS_LOADING });
  const payload = await callApi('/results.json');
  dispatch({ type: PRODUCTS_LOADED, payload });
};

export const filterByBrand = (brand, filter) => dispatch => {
  const key = brand.toLowerCase();
  dispatch({ type: FILTER_BY_BRAND, payload: { [key]: filter } });
};

export const filterByCategory = (category, filter) => dispatch => {
  dispatch({ type: FILTER_BY_CATEGORY, payload: { [category]: filter } });
};

export const filterBySearch = search => dispatch => {
  dispatch({ type: FILTER_BY_SEARCH, payload: { search } });
};
