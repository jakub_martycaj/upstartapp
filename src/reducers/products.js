import {
  PRODUCTS_LOADING,
  PRODUCTS_LOADED,
  PRODUCTS_ERROR,
  FILTER_BY_BRAND,
  FILTER_BY_CATEGORY,
  FILTER_BY_SEARCH,
} from '../actions/ActionTypes';

const initialState = {
  results: [],
  pagination: {},
  loading: false,
  categories: [
    {
      id: 0,
      name: 'Ceiling Fans',
    },
    { id: 1, name: 'Table Fans' },
  ],
  brands: ['Usha', 'Havells'],
  filter: {
    search: '',
    category: {},
    brand: {},
  },
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCTS_LOADING:
      return { ...state, loading: true };
    case PRODUCTS_LOADED:
      return { ...state, ...action.payload, loading: false };
    case PRODUCTS_ERROR:
      return { ...state, loading: false };
    case FILTER_BY_BRAND:
      const brand = { ...state.filter.brand, ...action.payload };
      return { ...state, filter: { ...state.filter, brand } };
    case FILTER_BY_CATEGORY:
      const category = { ...state.filter.category, ...action.payload };
      return { ...state, filter: { ...state.filter, category } };
    case FILTER_BY_SEARCH:
      return { ...state, filter: { ...state.filter, ...action.payload } };
    default:
      return state;
  }
};
