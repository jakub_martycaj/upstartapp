import { combineReducers } from 'redux';

import { productsReducer as products } from './products';

export const rootReducer = combineReducers({
  products,
});
