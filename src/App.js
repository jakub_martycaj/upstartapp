import React, { Component } from 'react';
import { Provider } from 'react-redux';

import { CategoryListPage } from './containers/CategoryList';
import { store } from './store';
import { Layout } from './components/Layout';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Layout>
          <CategoryListPage />
        </Layout>
      </Provider>
    );
  }
}

export default App;
