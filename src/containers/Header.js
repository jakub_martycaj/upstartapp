import { connect } from 'react-redux';

import { filterBySearch } from '../actions/products';
import { Header } from '../components/Header';

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  filterBySearch,
};

export const HeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
