import { connect } from 'react-redux';

import {
  fetchProducts,
  filterByBrand,
  filterByCategory,
} from '../actions/products';
import { CategoryList } from '../pages/CategoryList';
import { getFilteredProducts } from '../pages/CategoryList/selectors';

const mapStateToProps = state => ({
  pagination: state.products.pagination,
  products: getFilteredProducts(state),
  categories: state.products.categories,
  brands: state.products.brands,
});

const mapDispatchToProps = {
  fetchProducts,
  filterByBrand,
  filterByCategory,
};

export const CategoryListPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoryList);
