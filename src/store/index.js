import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import { rootReducer } from '../reducers';
const middleware = [
  thunk,
  process.env.NODE_ENV === 'development' && logger,
].filter(Boolean);

export const store = createStore(rootReducer, applyMiddleware(...middleware));
