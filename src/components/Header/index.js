import React, { Component } from 'react';
import { Grid, Col, Row } from 'react-flexbox-grid';
import styled from 'styled-components';

import { Container } from '../Container';
import { Button } from '../Button';
import { SearchBox } from '../SearchBox';

const HeaderContainer = styled.div`
  position: fixed;
  background-color: teal;
  height: 106px;
  display: flex;
  color: white;
  left: 0;
  right: 0;
`;

const logoUrl =
  'https://www.croma.com/medias/x,qcontext=bWFzdGVyfGltYWdlc3w2OTAwfGltYWdlL3BuZ3xpbWFnZXMvaGQwL2hhZS84ODY1NDc4MDQ5ODIyLnBuZ3xkZmFmZmU3MzFkYWI1MTIxOTQzOWFlMzI1NzNjNmIzNmUyMmQ2MzJmZjgzNjYyYzcyYTE5MWYyZmZmMzU2NDE5.pagespeed.ic.PVZR8i5gWD.png';

export class Header extends Component {
  state = {
    search: '',
  };

  onSearchChange = ({ target }) => {
    this.setState({ search: target.value });
  };

  onSearchSubmit = e => {
    e.preventDefault();
    this.props.filterBySearch(this.state.search);
  };

  render() {
    return (
      <HeaderContainer>
        <Container>
          <Grid>
            <Row middle="xs" between="xs">
              <Col>
                <img alt="Logo" src={logoUrl} />
              </Col>
              <Col xs={5}>
                <SearchBox
                  id="header-searchbox"
                  search={this.state.search}
                  onChange={this.onSearchChange}
                  onSubmit={this.onSearchSubmit}
                />
              </Col>
              <Col>
                <Button>View Cart</Button>
              </Col>
            </Row>
          </Grid>
        </Container>
      </HeaderContainer>
    );
  }
}
