import React from 'react';

import { Footer } from '../Footer';
import { HeaderContainer } from '../../containers/Header';
import { Container } from '../Container';

const ContentContainer = Container.extend`
  padding-top: 106px;
`;

export const Layout = ({ children }) => {
  return (
    <div>
      <HeaderContainer />
      <ContentContainer>{children}</ContentContainer>
      <Footer />
    </div>
  );
};
