import styled from 'styled-components';

export const Button = styled.button`
  display: block;
  width: 100%;
  text-align: center;
  background-color: #45a8a2;
  border-radius: 4px;
  padding: 0 10px;
  text-transform: uppercase;
  height: 40px;
  font-size: 1em;
  color: white;
  border-width: 0;
  cursor: pointer;
`;
