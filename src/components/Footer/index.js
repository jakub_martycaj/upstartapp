import React, { Component } from 'react';
import { Container } from '../Container';
import styled from 'styled-components';

const FooterContainer = styled.div`
  margin-top: 24px;
  padding: 24px 0;
  border-top: solid 1px lightgrey;
  font-size: 14px;
  color: grey;
`;

export class Footer extends Component {
  render() {
    return (
      <FooterContainer>
        <Container>
          Immense summer heat has made it essential to buy fans and coolers for
          homes and offices. You can easily buy best quality fans online at
          great prices. Here you can find a large range of fans and coolers from
          top brands such as Bajaj and Usha. When buying ceiling fans you need
          to keep some important factors in mind such as ceiling drop motor size
          blade pitch etc. This will ensure that you get a suitable fan for your
          home that not only gives you cool breeze but also helps you save on
          your electrical bills. Like electric fans air coolers are another way
          to stay cool in summers. These air coolers need to be placed near a
          window so that you are able to enjoy fresh air. If you are planning to
          buy fans for your homes or offices then be sure to check out the best
          deals offered here. Check out the huge range of fans from brands like
          Bajaj Bionaire Usha and Oster.
        </Container>
      </FooterContainer>
    );
  }
}
