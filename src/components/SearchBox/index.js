import React from 'react';
import styled from 'styled-components';

const SearchBoxWrapper = styled.div`
  position: relative;
  border: solid 1px #45a8a2;
  border-radius: 4px;
`;

const SearchInput = styled.input.attrs({
  type: 'text',
  placeholder: 'Search products',
})`
  border-radius: 4px;
  width: 100%;
  padding: 0px 16px;
  height: 50px;
  border-width: 0;
  font-size: 16px;
`;

const SearchButton = styled.button`
  position: absolute;
  right: 0;
  height: 50px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  border-width: 0;
  text-transform: uppercase;
  font-size: 16px;
  padding: 0 16px;
  background-color: #45a8a2;
  color: white;
  cursor: pointer;
`;

export const SearchBox = ({ id, onSubmit, onChange }) => {
  return (
    <SearchBoxWrapper>
      <form id={id} name={id} onSubmit={onSubmit}>
        <SearchInput onChange={onChange} />
        <SearchButton onSubmit={onSubmit}>Go</SearchButton>
      </form>
    </SearchBoxWrapper>
  );
};
