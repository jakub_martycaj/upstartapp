import 'whatwg-fetch';

export const callApi = async path => {
  const response = await fetch(path);
  const json = await response.json();
  return json;
};
